" -*- mode: vimrc -*-
"vim: ft=vim


" dotspaceneovim/user-config {{{
  "Configuration block for user code.
  "This function is called at the very end of SpaceNeovim initialization after
  "layers configuration.
  "This is the place where most of your configurations should be done. Unless
  "it is explicitly specified that a variable should be set before a package is
  "loaded, you should place your code here."
  " Set default colorscheme to wombat256mod and the background to dark
  set background=dark
  try
    colorscheme wombat256mod
  catch
  endtry

" }}}
