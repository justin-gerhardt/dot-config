" -*- mode: vimrc -*-
"vim: ft=vim


" dotspaceneovim/layers {{{
  "Configuration Layers declaration.
  "You should not put any user code in this block.
  let g:dotspaceneovim_configuration_layers += [
  \  '+checkers/syntastic'
  \, '+completion/snippets'
  \, '+tools/terminal'
  \, '+lang/css'
  \, '+lang/php'
  \]

" }}}

