# .config

This is basically my ~/.config folder. Inspired by
https://github.com/rafi/.config

(i still need to catch up)

for now its just this. i will ignore things and add them
as I go.

To setup, for now, fork this repo, then clone that to `~/.config`.
but chances are, you already have that. so in that case run:

```bash
cd ~/.config
git init
git remote add [your fork/clone url]
git fetch
```

At this point, you can add any files you want, ignore any, or whatever.
I do have my neovim and tmux configs in here too. If you want to use
them run this :

```bash
cd ~/.config
git submodule init
git submodule update
```
which will set those up. to get going on those, refer to those repos for info.

- https://gitlab.com/autoferrit/dot-vim
- https://gitlab.com/autoferrit/dot-tmux

