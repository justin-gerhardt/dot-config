# Setup fzf
# ---------
if [[ ! "$PATH" == */home/shawn/.fzf/bin* ]]; then
  export PATH="$PATH:/home/shawn/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/shawn/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/shawn/.fzf/shell/key-bindings.zsh"

