## PLUGINS

# base
zplug 'zplug/zplug', hook-build:'zplug --self-manage'
zplug "b4b4r07/zplug"
zplug "zsh-users/zsh-history-substring-search", defer:2
zplug "zsh-users/zsh-completions", defer:2
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-history-substring-search"
zplug "mollifier/zload"

# python
# zplug "plugins/python", from:oh-my-zsh
# zplug "plugins/pep8", from:oh-my-zsh
# zplug "plugins/autopep8", from:oh-my-zsh
# zplug "plugins/urltools", from:oh-my-zsh
# zplug "plugins/fabric", from:oh-my-zsh
#zplug "plugins/pip", from:oh-my-zsh

# fzf
if [[ ! -d $HOME/.fzf ]]; then
    git clone https://github.com/junegunn/fzf.git $HOME/.fzf
    ./$HOME/.fzf/install
fi
sourceMe "${XDG_CONFIG_HOME}/.fzf.zsh"

#zplug "junegunn/fzf-bin", from:gh-r, as:command, rename-to:fzf

# git
zplug "plugins/git", from:oh-my-zsh

# php

# js
zplug "lukechilds/zsh-nvm"
zplug "plugins/gulp", from:oh-my-zsh
zplug "plugins/npm", from:oh-my-zsh

# composer
zplug "plugins/composer", from:oh-my-zsh

# docker
zplug "plugins/docker", from:oh-my-zsh
zplug "plugins/docker-compose", from:oh-my-zsh

# system
zplug "plugins/httpie", from:oh-my-zsh
zplug "rupa/z", use:z.sh
#zplug "knu/z", use:z.sh, nice:10
zplug "andrewferrier/fzf-z", from:github

# misc
zplug "plugins/safe-paste", from:oh-my-zsh
zplug "plugins/zsh_reload", from:oh-my-zsh
zplug "plugins/vi-mode", from:oh-my-zsh

# tmux
zplug "plugins/tmux", from:oh-my-zsh
zplug "plugins/tmuxinator", from:oh-my-zsh

#go
zplug "plugins/golang",   from:oh-my-zsh

# fun
zplug "plugins/chucknorris", from:oh-my-zsh

# zsh-syntax-highlighting must be loaded
# after executing compinit command and sourcing other plugins
# (If the defer tag is given 2 or above, run after compinit command)
zplug "zsh-users/zsh-syntax-highlighting", defer:2

# prompt
zplug mafredri/zsh-async, from:github
zplug sindresorhus/pure, use:pure.zsh, from:github, as:theme

#zplug "bhilburn/powerlevel9k", use:powerlevel9k.zsh-theme

# update/install
if ! zplug check --verbose; then
	printf "Install? [y/N]: "
	if read -q; then
		echo
		zplug install
	fi
fi
