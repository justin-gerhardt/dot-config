
# test if dir present, if so, add to PATH
# whenever run, add to left first, which gets priority
# -----------------------------------------------------------
# export addPath() {
    # [[ -d "${1}" ]] && export PATH="${1}:${PATH}"
# }

# easily source files
# export sourceMe() {
#    [[ -f "${1}" ]] && source "${1}"
# }

sourceMe "${ZSH_DIR}/zplug_init.zsh"
sourceMe "${ZSH_DIR}/plugin_config.zsh"
sourceMe "${ZSH_DIR}/plugins.zsh"


# things in utils need to be after setting prompt in theme
configs=(completions aliases utils functions)
for conf in $configs; do
  item="${ZSH_DIR}/${conf}.zsh"
 	#echo "!!! SOURCING ${item}"
 	[[ -e ${item} ]] && sourceMe ${item}
 	#echo "!!! DONE SOURCING ${item}"
done
unset item
unset conf

# If SSH session and not running tmux, list tmux sessions on log-in.
if [ -n "$SSH_TTY" ] && [ -z "$TMUX" ] && hash tmux 2>/dev/null; then
    tmux list-sessions 2>/dev/null | sed 's/^/# tmux /'
fi

#if zplug check --verbose || zplug install; then
if zplug check || zplug install; then
  zplug load
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

neofetch --block_range 1 8 --bold on --uptime_shorthand on --gtk_shorthand on

# vim: set ft=sh ts=2 sw=0 tw=80 noet :
