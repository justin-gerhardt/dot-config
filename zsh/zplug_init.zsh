if [[ ! -d ~/.zplug ]]; then
	git clone https://github.com/zplug/zplug ~/.zplug
fi

mkdir -p ~/.zplug/cache

# init
sourceMe ~/.zplug/init.zsh
#zplug update --self
