#!/usr/bin/env bash
OS=nil
case $(uname) in
	Darwin)
		OS=osx
		;;
	Linux)
		OS=linux
		;;
	MINGW)
		OS=win
		;;
esac
export OS
