# language crap, because, explicivity.
# -----------------------------------------------------------
UTF8="en_US.UTF-8"
export        LANG="en_US.UTF-8"
export  LC_COLLATE="en_US.UTF-8"
export    LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export  LC_NUMERIC="en_US.UTF-8"
export     LC_TIME="en_US.UTF-8"
export      LC_ALL="en_US.UTF-8"

# XDG
# -----------------------------------------------------------
export XDG_CONFIG_HOME="${HOME}/.config"
export  XDG_CACHE_HOME="${HOME}/.cache"
export   XDG_DATA_HOME="${HOME}/.local/share"
export XDG_RUNTIME_DIR="$XDG_CACHE_HOME"
export      ZPLUG_HOME="${HOME}/.zplug"
export         ZSH_DIR="${XDG_CONFIG_HOME}/zsh"

# XDG_CACHE_HOME
# -----------------------------------------------------------
export     XDG_DESKTOP_DIR="$HOME/Desktop"
export    XDG_DOWNLOAD_DIR="$HOME/Downloads"
export   XDG_TEMPLATES_DIR="$HOME/Templates"
export XDG_PUBLICSHARE_DIR="$HOME/Public"
export   XDG_DOCUMENTS_DIR="$HOME/Documents"
export       XDG_MUSIC_DIR="$HOME/Music"
export    XDG_PICTURES_DIR="$HOME/Pictures"
export      XDG_VIDEOS_DIR="$HOME/Videos"

#
# MISC
# -----------------------------------------------------------
# https://github.com/zealdocs/zeal/issues/172
export QT_QPA_PLATFORMTHEME=gnome
export              INPUTRC="$XDG_CONFIG_HOME/bash/inputrc"
export          RXVT_SOCKET="$XDG_RUNTIME_DIR/urxvtd"
export            GNUPGHOME="$XDG_CONFIG_HOME/gnupg"
export   PASSWORD_STORE_DIR="$HOME/docs/pass/"
export       URXVT_PERL_LIB="$XDG_CONFIG_HOME/urxvt/ext"
export       MYSQL_HISTFILE="$XDG_CACHE_HOME/mysql_history"
export        WAKATIME_HOME="$XDG_DATA_HOME/wakatime"
export         WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"

# ZSH settings
# -----------------------------------------------------------
export HISTFILE="${HOME}/.zsh_history"
export SAVEHIST=10000

# Settings
# -----------------------------------------------------------
export LESS="-FiQMXR"
export LESSCHARSET="UTF-8"
alias grep="grep --color=auto --exclude-dir=.git"

# set home for various apps
# -----------------------------------------------------------
hash bspwm      2>/dev/null && export      BSPWM_SOCKET="$XDG_RUNTIME_DIR/bswpm-socket"
hash http       2>/dev/null && export HTTPIE_CONFIG_DIR="$XDG_CONFIG_HOME/httpie"
hash wget       2>/dev/null && export            WGETRC="$XDG_CONFIG_HOME/wget/config"
hash psql       2>/dev/null && export            PSQLRC="$XDG_CONFIG_HOME/psql/config"
hash pgcli      2>/dev/null && export           PGCLIRC="$XDG_CONFIG_HOME/pgcli/config"
hash gimp       2>/dev/null && export   GIMP2_DIRECTORY="$XDG_CONFIG_HOME/gimp-2.8"
hash virtualbox 2>/dev/null && export    VBOX_USER_HOME="$XDG_CONFIG_HOME/virtualbox"
hash mplayer    2>/dev/null && export      MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
hash mpv        2>/dev/null && export          MPV_HOME="$XDG_CONFIG_HOME/mpv"
hash vagrant    2>/dev/null && export      VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
hash sshrc      2>/dev/null && export           SSHHOME="$XDG_CONFIG_HOME/sshrc"

# hack to fix `nice` on WSL
# https://github.com/Microsoft/BashOnWindows/issues/1887#issuecomment-294297758
# -----------------------------------------------------------
if grep -q Microsoft /proc/version; then
    unsetopt BG_NICE
fi

# test if dir present, if so, add to PATH
# whenever run, add to left first, which gets priority
# -----------------------------------------------------------
addPath() {
    [[ -d "${1}" ]] && export PATH="${1}:${PATH}"
}

# easily source files
sourceMe() {
   [[ -f "${1}" ]] && source "${1}"
}

# if [[ -d $XDG_CONFIG_HOME/bin ]]; then
    # export PATH="$XDG_CONFIG_HOME/bin:$PATH"
# fi

# OSX
# -----------------------------------------------------------
if [[ "$OSTYPE" == "darwin"* ]]; then
    # export XINITRC="$XDG_CONFIG_HOME"/xorg/xinitrc_osx
    # HomeBrew
    addPath "/usr/local/sbin"
    addPath "/usr/local/bin"
    export MANPATH=/opt/local/share/man:$MANPATH
fi

# local bins
# -----------------------------------------------------------
addPath "${HOME}/.local/bin"
addPath "${XDG_CONFIG_HOME}/bin"

# go
# -----------------------------------------------------------
[[ ! -d "${HOME}/go" ]] && mkdir -p "${HOME}/go/bin"
[[ ! -d "${HOME}/code" ]] && mkdir -p "${HOME}/code/bin"
# GOPATH="path1:path2:path(n)"
export GOPATH="${HOME}/go:${HOME}/code"
addPath ${HOME}/go/bin
addPath ${HOME}/code/bin

# php
# -----------------------------------------------------------
export COMPOSER_HOME="$XDG_CACHE_HOME/composer"
addPath "${COMPOSER_HOME}/bin"

# z
# -----------------------------------------------------------
export _Z_DIR="$XDG_CACHE_HOME/z"
[ -d "${_Z_DIR}" ] || mkdir -p "${_Z_DIR}"
export _Z_DATA="${_Z_DIR}/data"

# python
# -----------------------------------------------------------
export IPYTHONDIR="$XDG_CONFIG_HOME"/ipython

# homebrew
# -----------------------------------------------------------
export HOMEBREW_GITHUB_API_TOKEN="b0be90b49823876173b1f9b32fca799327ead69e"

# Node/NPM
# -----------------------------------------------------------
export NVM_DIR="${HOME}/.nvm"
sourceMe "/usr/share/nvm/init-nvm.sh" # This loads nvm
# if we haves is, uses it
if hash nvm 2>/dev/null; then
    def="$(cat ${HOME}/.nvm/alias/default)"
    export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
    export MANPAGER="nvim -c 'set ft=man' -"
    addPath "$HOME~/.nvm/versions/node/${def}/bin"
fi

# Rust Cargo configuration
# -----------------------------------------------------------
addPath "${HOME}/.cargo/bin"

# Always install ruby gems local to the user
# -----------------------------------------------------------
if hash gem 2>/dev/null; then
    export GEMRC="${XDG_CONFIG_HOME}/gemrc/config"
    export GEM_HOME="${HOME}/.local/lib/ruby/gems/$(ruby -e 'puts RbConfig::CONFIG["ruby_version"]')"
    export GEM_SPEC_CACHE="${XDG_CACHE_HOME}/gem/specs"
    addPath "${GEM_HOME}/bin"
fi

# fzf
# -----------------------------------------------------------
sourceMe "${XDG_CONFIG_HOME}/.fzf.zsh"

_gen_fzf_default_opts() {
    local base03="234"
    local base02="235"
    local base01="240"
    local base00="241"
    local base0="244"
    local base1="245"
    local base2="254"
    local base3="230"
    local yellow="136"
    local orange="166"
    local red="160"
    local magenta="125"
    local violet="61"
    local blue="33"
    local cyan="37"
    local green="64"

    local baseopts='
        --bind=ctrl-d:page-down,ctrl-u:page-up,y:yank
        --inline-info
        --header="type to filter"
        --prompt=" >>> "
        --ansi
        --tabstop=4
        --color=dark
    '

  # Solarized Dark color scheme for fzf
  export FZF_DEFAULT_OPTS="${baseopts}
    --color fg:-1,bg:-1,hl:$blue,fg+:$base2,bg+:$base02,hl+:$blue
    --color info:$yellow,prompt:$yellow,pointer:$base3,marker:$base3,spinner:$yellow
  "
  ## Solarized Light color scheme for fzf
  #export FZF_DEFAULT_OPTS="
  #  --color fg:-1,bg:-1,hl:$blue,fg+:$base02,bg+:$base2,hl+:$blue
  #  --color info:$yellow,prompt:$yellow,pointer:$base03,marker:$base03,spinner:$yellow
  #"
}

if hash fzf 2>/dev/null; then
     _gen_fzf_default_opts
fi

# android sdk
# -----------------------------------------------------------
if [ -d "/usr/local/opt/android-sdk" ]; then
    export ANDROID_HOME="/usr/local/opt/android-sdk"
    addPath "${HOME}/android/tools"
    addPath "${HOME}/android/platform-tools"
fi

# powerline
# -----------------------------------------------------------
export POWERLINE_CONFIG_COMMAND="/usr/local/bin/powerline-config"

# appengine
# addPath "${HOME}/code/vendor/go_appengine"

# riskalyze
# -----------------------------------------------------------
addPath "${HOME}/code/src/github.com/riskalyze.com/riskalyze-devironment/bin"

# intellij
# -----------------------------------------------------------
_PHPSTORM="${HOME}/opt/phpstorm/bin"
addPath "${_PHPSTORM}"

[[ -f $HOME/.zshenv.private ]] && sourceMe $HOME/.zshenv.private

# vim: set ft=sh ts=2 sw=2 tw=80 noet :
