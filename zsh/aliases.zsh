# gitlab.com/autoferrit zsh aliases

# Carry over aliases to the root account when using sudo
alias sudo='sudo '

# Use GNU tools on OSX instead of BSD
# gls comes from brew
if hash gls 2>/dev/null; then
	LS="gls"
else
	LS="ls"
fi

# making history shorter
if hash fzf 2>/dev/null; then
	  cat ~/.bash_history > /tmp/history
	  cat ~/.zsh_history >> /tmp/history
    alias h='eval "$(cat /tmp/history | sort | uniq | fzf -e)"'
else
    alias h=history
fi

# Listing directory contents
alias  ls=$LS' --color=auto --group-directories-first'
alias  la='ls -Falh'
alias lad='ls -Falh | grep ^d'
alias   l='ls -FaC'
alias  ll='ls -Fal'
alias lsd='ls -Gal | grep ^d'
unset LS

alias grep="grep --color=auto --exclude-dir=.git"

# File and directory find
alias f='find . -name '
alias ff='find . -type f -name '
alias fd='find . -type d -name '

# Head and tail will show as much possible without scrolling
#alias h='ghead -n $((${LINES:-12}-4))'
#alias t='gtail -n $((${LINES:-12}-4)) -s.1'

# Git
alias   gb='git branch'
alias   ga='git add'
alias   gc='git commit -m'
alias  gco='git checkout'
alias gcob='git checkout -b'
alias  gd='git diff'
alias  gds='git diff --cached'
alias  gfl='git fetch --prune && git lg -15'
alias   gf='git fetch --prune'
alias  gfa='git fetch --all --tags --prune'
alias   gs='git status -sbu'
alias  gst='git status -u'
alias   gl='git lg -15'
alias  gll='git lg'
alias  gld='git lgd -15'
compdef _git gb=git-branch
compdef _git ga=git-add
compdef _git gc=git-commit
compdef _git gco=git-checkout
compdef _git gcob=git-checkout
compdef _git gd=git-diff
compdef _git gds=git-diff
compdef _git gf=git-fetch
compdef _git gfa=git-fetch
compdef _git gs=git-status
compdef _git gst=git-status
compdef _git gl=git-log
compdef _git gll=git-log
compdef _git gld=git-log

# Docker
alias dk="docker"
alias dkps="dk ps -a"
alias dki="dk images"
alias dkinfo="dkps && echo '======' && dki"
alias dks="dk stop"
alias dkrm="dk rm"
dkrma() {
	#if [ "linux" = "${OS}" ]; then
	#    dks $(sudo docker ps -a -q) && dkrm $(sudo docker ps -a -q)
	#else
	dks $(docker ps -a -q) && dkrm $(docker ps -a -q)
	#fi
}
alias dkrmi="dk rmi"
alias dklog="dk logs"
dkbash() {
	dk exec -it $1 bash
}
dkip() {
	dk inspect $1 | grep IPAddress | cut -d '"' -f 4
}

# docker compose
alias dc="docker-compose"
if [ "linux" = "${OS}" ]; then
	alias dc="sudo docker-compose"
fi
alias dcup="dc up"
alias dcupd="dc up -d"
dcreset() {
	if [ ! -f docker-compose.yml ]; then
		echo "Could not find a 'docker-compose.yml' file"
	else
		dkrma && dcup
		cleaned=$(dkps | grep tcp)
		oldIFS="$IFS"
		IFS=', '
		docks=($cleaned)
		IFS=$oldIFS
		for dock in "${docks[@]}"; do
			dock=${dock} | tr -s ' '
			id=${dock} | cut -d ' ' -f 1
			ip="ip" #$(dkip ${id})
            name="name" #${dock} | rev | cut -d ' ' -f 1 | rev
            image="image" #${dock} | cut -d ' ' -f 2
            echo "${id}" # ${ip} ${name} ${image}"
		done
	fi
}

# emacs
e() {
	echo "emacsclient -c > ""$@"
	emacsclient -c "$@" &
}

# vim
if hash nvim 2>/dev/null; then
	# all NEO
  export EDITOR="$(which nvim)"
else
  echo "NO NVIM"
	# if we still dont have vim, it means no vim nor nvim. vi?
	if hash vim 2>/dev/null; then
		# point nvim to vim
    export EDITOR="$(which vim)"
	else
		# still nothing, use vi
    export EDITOR="$(which vi)"
	fi
fi

alias vim="${EDITOR}"
alias vi="${EDITOR}"
alias v="${EDITOR}"

alias vimdiff="vim -d"
alias suvim="sudo -E vim"

if hash fzf 2>/dev/null; then
    if [[ "$TERM" = "screen" ]] && [[ -n "$TMUX" ]]; then
        alias v="${EDITOR} \"\$(fzf-tmux -l30)\""
    else
        alias v="${EDITOR} \"\$(fzf -l30)\""
    fi
fi
# A quick way to get out of current directory
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'

# OSX
if [[ "$OSTYPE" == "darwin"* ]]; then
    alias showfiles="defaults write com.apple.finder AppleShowAllFiles -bool true && killall Finder"
    alias hidefiles="defaults write com.apple.finder AppleShowAllFiles -bool false && killall Finder"
fi

# TMUX
# because some times it doesnt find the config path
alias tmux="tmux -f ${XDG_CONFIG_HOME}/.tmux.conf"

#  vim: set ft=sh ts=2 sw=2 tw=80 noet :
