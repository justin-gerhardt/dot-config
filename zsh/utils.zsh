# github.com/rafi/.config

# Set default browser
if [[ "$OSTYPE" == "darwin"* ]]; then
	export BROWSER="open"
else
	export BROWSER="$(which firefox)"
fi

# Git prompt helpers
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWCOLORHINTS=0
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_SHOWUPSTREAM="auto"

# vim: set ft=sh ts=2 sw=2 tw=80 et :
