" -*- mode: vimrc -*-
"vim: ft=vim

" dotspaceneovim/user-init {{{

" Enable line numbers
  set number
  " Set 7 lines to the cursor - when moving vertically using j/k
  set scrolloff=10
  " Use relative line numbers. Options are:
  " - relativenumber/norelativenumber
  " - number/nonumber
  set norelativenumber
  " Always show the status line
  set laststatus=2

"-------- TAGS SETTINGS --------------------------------
let g:easytags_events = ['BufReadPost', 'BufWritePost']
let g:easytags_async = 1
let g:easytags_dynamic_files = 2
let g:easytags_resolve_links = 1
let g:easytags_suppress_ctags_warning = 1
let g:tagbar_autoclose=2

"nmap <silent> <leader>b :TagbarToggle<CR>
"autocmd BufEnter * nested :call tagbar#autoopen(0)


"-----------TMUX SETTINGS--------------
let g:tmux_navigator_save_on_switch = 2
" is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
"     | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
"     bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
"     bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
"     bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
"     bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
"     bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

" +lang/css
let g:sp_css_indentatio	= 2

" }}}

